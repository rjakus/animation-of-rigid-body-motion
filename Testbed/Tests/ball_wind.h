#ifndef BALL_WIND_H
#define BALL_WIND_H
#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f
#define PI 3.14159265


#include <Box2D/Common/b2Settings.h>
#include <math.h>

int i=1;

 class ball_wind : public Test
  {
       int x_os[100];

       //class member variable to keep track of bodie
       b2Body* dynamicBody[100];
             

        public:
                      
            ball_wind() {  
          
            //ball
            b2BodyDef myBodyDef1;
            myBodyDef1.type = b2_dynamicBody; //this will be a dynamic body
            myBodyDef1.position.Set(-23, 0); //a little to the left
            myBodyDef1.angle = 0; //set the starting angle

            dynamicBody[0] = m_world->CreateBody(&myBodyDef1);

            b2CircleShape circleShape;
            circleShape.m_p.Set(0, 0); //position, relative to body position
            circleShape.m_radius = 1; //radius

            b2FixtureDef myFixtureDef;
            myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
            myFixtureDef.density = 1.0f;
            myFixtureDef.friction = 0.7f; //rotate
            myFixtureDef.restitution = 0.8f; //bounce

            dynamicBody[0]->CreateFixture(&myFixtureDef); //add a fixture to the body
            
            
            int degrees=70, power=50;
            dynamicBody[0]->ApplyLinearImpulse( b2Vec2(cos(degrees * (PI / 180)) * power,sin(degrees * (PI / 180)) * power),dynamicBody[0]->GetWorldCenter(), false);
 

              //a static BOX to put things in it
            b2BodyDef pod;
            pod.position.Set(0.0f, 0.0f);
            b2Body* body = m_world->CreateBody(&pod);

            b2EdgeShape edge[4];

            edge[0].Set(b2Vec2(-25.0f, 0.0f), b2Vec2(25.0f, 0.0f));
            body->CreateFixture(&edge[0], 0.0f);
         
            edge[1].Set(b2Vec2(-25.0f, 15.0f), b2Vec2(25.0f, 15.0f));
            body->CreateFixture(&edge[1], 0.0f);
          
            edge[2].Set(b2Vec2(-25.0f, 0.0f), b2Vec2(-25.0f, 15.0f));
            body->CreateFixture(&edge[2], 0.0f);
          
            edge[3].Set(b2Vec2(25.0f, 0.0f), b2Vec2(25.0f, 15.0f));
            body->CreateFixture(&edge[3], 0.0f);

            } 
  void CreateCircle()
        {
         if(i<100)
            {
             b2BodyDef myBodyDef1;
            
            myBodyDef1.type = b2_dynamicBody; //this will be a dynamic body
            myBodyDef1.position.Set(-23, 1); //a little to the left
            myBodyDef1.angle = 0; //set the starting angle

            dynamicBody[i] = m_world->CreateBody(&myBodyDef1);

            b2CircleShape circleShape;
    
            circleShape.m_radius = 1; //radius

            b2FixtureDef myFixtureDef;
            myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
            myFixtureDef.density = 1.0f;
            myFixtureDef.friction = 0.7f; //rotate
            myFixtureDef.restitution = 0.8f; //bounce

            dynamicBody[i]->CreateFixture(&myFixtureDef); //add a fixture to the body

            int degrees=70, power=50;
            dynamicBody[i]->ApplyLinearImpulse( b2Vec2(cos(degrees * (PI / 180)) * power,sin(degrees * (PI / 180)) * power),dynamicBody[i]->GetWorldCenter(), false);

            i=i+1;
          }
          else {i=0;}
          
        }

  void Step(Settings* settings)
        {
     
            //run the default physics and rendering
            Test::Step(settings); 

            for(int32 j = 0; j<i; j++)
                  {
                    b2Vec2 position=dynamicBody[j]->GetWorldCenter();
                    x_os[j]=position.x;
                  }

                  //show some text in the main screen
            m_debugDraw.DrawString(10, m_textLine, "With 'q' aplly wind to the left <-");
            m_textLine += DRAW_STRING_NEW_LINE;
            m_debugDraw.DrawString(10, m_textLine, "With 'e' aplly wind to the right -> ");
            m_textLine += DRAW_STRING_NEW_LINE;
            m_debugDraw.DrawString(10, m_textLine, "With 'c' create new ball ");
            m_textLine += DRAW_STRING_NEW_LINE;

            //get the position of the ball  
            
        }
    
  void apllyforceq()
      {
        for(int32 j = 0; j<i; j++)
          {
          if(x_os[j]>=0){
              dynamicBody[j]->ApplyForce(b2Vec2(-70  ,10), dynamicBody[j]->GetWorldCenter() , false );
      
                    }
          if(x_os[j]<0){
              dynamicBody[j]->ApplyForce(b2Vec2(-70 ,10), dynamicBody[j]->GetWorldCenter() , false );
                    }
          }
      }

  void apllyforcee()
      {
         for(int32 j = 0; j<i; j++)
          {
          if(x_os[j]>=0){
              dynamicBody[j]->ApplyForce(b2Vec2(40-x_os[j]  ,10), dynamicBody[j]->GetWorldCenter() , false );
      
                    }
          if(x_os[j]<0){
              dynamicBody[j]->ApplyForce(b2Vec2(-1*x_os[j]+40  ,10), dynamicBody[j]->GetWorldCenter() , false );
                    }
          }
      }

 void Keyboard(unsigned char key)
         {
                    switch (key)
                    {
                      case 'q':
                          apllyforceq();

                      case 'e':
                          apllyforcee();
              
                        break;  

                      case 'c':
                          CreateCircle();
                        break;

                     
                      default:
                        //run default behaviour
                      Test::Keyboard(key);
                    }
           }


        static Test* Create()
        {
            return new ball_wind;
        }
  };
  
  #endif