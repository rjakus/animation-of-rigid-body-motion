
#ifndef IFORCE2D_BUOYANCY_FUNCTIONS_H
#define IFORCE2D_BUOYANCY_FUNCTIONS_H
#define USE_LINEAR_IMPULSE


#include <exception>
#include <iostream>

#include <map>
#include <stdint.h>

// This file contains the support functions for finding the intersecting portion
// of two polygon fixtures.

#include <set>
#include <vector>
typedef std::pair<b2Fixture*, b2Body*> fixturePair;

struct b2BodyUserData {
	std::vector<std::vector<b2Vec2> > allFixes;
	std::vector<b2Vec2> allFixesCombined;
	std::map<b2Fixture*, uint32> fluidContacts;
};

b2BodyUserData* GetUserData(b2Body* body) {
	static std::vector<b2BodyUserData> buds = std::vector<b2BodyUserData>();
	uintptr_t index = (uintptr_t)body->GetUserData();
	if(index == 0) {
		b2BodyUserData bud;
		bud.allFixes = std::vector<std::vector<b2Vec2> >();
		bud.allFixesCombined = std::vector<b2Vec2>();
		bud.fluidContacts = std::map<b2Fixture*, uint32>();
		buds.push_back(bud);
		index = buds.size();
		body->SetUserData((void*)index);
	}
	return &buds[index - 1];
}

struct b2FixtureUserData {
	bool isFluid;
};

b2FixtureUserData* GetUserData(b2Fixture* fix) {
	return (b2FixtureUserData*)fix->GetUserData();
}

bool IsFluid(b2Fixture* fix) {
	b2FixtureUserData* fud = GetUserData(fix);
	if(fud == NULL) {
		return false;
	}
	return fud->isFluid;
}

bool AddFluidContact(b2Fixture* fix, b2Body* body) {
	b2BodyUserData* bud = GetUserData(body);
	if(!bud->fluidContacts.count(fix)) {
		bud->fluidContacts.insert(std::pair<b2Fixture*, uint32>(fix, 1));
		return true;
	}
	++(bud->fluidContacts[fix]);
	return false;
}

bool RemoveFluidContact(b2Fixture* fix, b2Body* body) {
	b2BodyUserData* bud = GetUserData(body);
	if(bud->fluidContacts.count(fix)) {
		if(--(bud->fluidContacts[fix]) == 0) {
			bud->fluidContacts.erase(fix);
			return true;
		}
	}
	return false;
}


void PrintNewLine(int32 numNewLines = 1) {
	for(int32 i = 0; i < numNewLines; i++) {
		std::cout << std::endl;
	}
}

void PrintVec2(const b2Vec2 in, const int32 numNewLines = 1) {
	std::cout << "(" << in.x << ", " << in.y << ")";
	PrintNewLine(numNewLines);
}

void PrintPoly(const std::vector<b2Vec2>& in, const char* const name, const int32 numNewLines = 1) {
	if(name != NULL && name[0] != '\0') {
		std::cout << name << " = ";
	}
	std::cout << "[";
	for(int32 i = 0; i < in.size() - 1; i++) {
		PrintVec2(in[i], 0);
		std::cout << " ";
	}
	if(in.size() > 0) {
		PrintVec2(in[in.size() - 1], 0);
	}
	std::cout << "]";
	PrintNewLine(numNewLines);
}

static b2Vec2 ComputeCentroid(std::vector<b2Vec2> vs, float32& area) {
	int32 count = (int32)vs.size();
	b2Assert(count >= 3);

	b2Vec2 c; c.Set(0.0f, 0.0f);
	area = 0.0f;

	// pRef is the reference point for forming triangles.
	// Its location doesn't change the result (except for rounding error).
	b2Vec2 pRef(0.0f, 0.0f);

	const float32 inv3 = 1.0f / 3.0f;

	for(int32 i = 0; i < count; ++i) {
		// Triangle vertices.
		b2Vec2 p1 = pRef;
		b2Vec2 p2 = vs[i];
		b2Vec2 p3 = i + 1 < count ? vs[i + 1] : vs[0];

		b2Vec2 e1 = p2 - p1;
		b2Vec2 e2 = p3 - p1;

		float32 D = b2Cross(e1, e2);

		float32 triangleArea = 0.5f * D;
		area += triangleArea;

		// Area weighted centroid
		c += triangleArea * inv3 * (p1 + p2 + p3);
	}

	// Centroid
	if(area > b2_epsilon)
		c *= 1.0f / area;
	else
		area = 0.0f;
	return c;
}

static b2Vec2 ComputeCentroid(std::vector<std::vector<b2Vec2> > vs, float32& area) {
	int32 count = (int32)vs.size();
	std::vector<b2Vec2> centroids = std::vector<b2Vec2>(count);
	std::vector<float32> areas = std::vector<float32>(count);

	for(int32 i = 0; i < count; i++) {
		centroids[i] = ComputeCentroid(vs[i], areas[i]);
		area += areas[i];
	}

	b2Vec2 c = b2Vec2_zero;
	for(int32 i = 0; i < count; i++) {
		c += areas[i] * centroids[i];
	}

	if(area > b2_epsilon)
		c *= 1.0f / area;
	else
		area = 0.0f;
	return c;
}

bool inside(b2Vec2 cp1, b2Vec2 cp2, b2Vec2 p) {
	return (cp2.x - cp1.x)*(p.y - cp1.y) > (cp2.y - cp1.y)*(p.x - cp1.x);
}

b2Vec2 intersection(b2Vec2 cp1, b2Vec2 cp2, b2Vec2 s, b2Vec2 e) {
	b2Vec2 dc(cp1.x - cp2.x, cp1.y - cp2.y);
	b2Vec2 dp(s.x - e.x, s.y - e.y);
	float n1 = cp1.x * cp2.y - cp1.y * cp2.x;
	float n2 = s.x * e.y - s.y * e.x;
	float n3 = 1.0f / (dc.x * dp.y - dc.y * dp.x);
	return b2Vec2((n1*dp.x - n2*dc.x) * n3, (n1*dp.y - n2*dc.y) * n3);
}

void GetPolygonVerts(const b2Fixture* in, std::vector<b2Vec2>& out) {
	b2PolygonShape* poly = (b2PolygonShape*)in->GetShape();
	for(int32 i = 0; i < poly->GetVertexCount(); i++) {
		out.push_back(poly->GetVertex(i));
	}
}

int32 NumVertsInPolygonCircle = 32;

void PolygonizeCircle(const b2Fixture* in, std::vector<b2Vec2>& out) {
	b2CircleShape* circle = (b2CircleShape*)in->GetShape();
	float32 interval = 2.0f * b2_pi / NumVertsInPolygonCircle;
	std::vector<b2Vec2> verts = std::vector<b2Vec2>(NumVertsInPolygonCircle);
	b2Rot rot;
	for(int32 i = 0; i < NumVertsInPolygonCircle; i++) {
		rot.Set(i*interval);
		verts[i] = (circle->m_p + circle->m_radius*b2Vec2(rot.c, rot.s));
	}
	out = verts;
}

bool GetShapeVerts(const b2Fixture* in, std::vector<b2Vec2>& out) {
	switch(in->GetShape()->GetType()) {
	case b2Shape::e_polygon:
		GetPolygonVerts(in, out);
		break;
	case b2Shape::e_circle:
		PolygonizeCircle(in, out);
		break;
	default:
		return false;
	}
	return true;
}

uint32 GetFixtureCount(b2Fixture* fix) {
	uint32 count = 0;
	while(fix != NULL) {
		fix = fix->GetNext();
		++count;
	}
	return count;
}

bool SamePoint(b2Vec2 a, b2Vec2 b) {
	return b2DistanceSquared(a, b) < 0.5f * b2_linearSlop;
}

size_t Wrap(size_t n, size_t lo, size_t hi) {
	n -= lo;
	hi -= lo;
	n %= hi;
	n += lo;
	return n;
}

std::vector<b2Vec2> CopyRangeWrap(std::vector<b2Vec2>& in, size_t start, size_t size) {
	std::vector<b2Vec2> np = std::vector<b2Vec2>();
	for(size_t i = Wrap(start, 0, in.size()); np.size() < size; i = Wrap(i + 1, 0, in.size())) {
		np.push_back(in[i]);
	}
	return np;
}

void MergePolys(std::vector<std::vector<b2Vec2> >& in) {
	std::vector<std::vector<b2Vec2> > out = std::vector<std::vector<b2Vec2> >();
	std::vector<b2Vec2> poly = std::vector<b2Vec2>();
	while(!in.empty()) {
		poly = in.back();
		in.pop_back();
		for(size_t i = in.size(); i-- > 0;) {
			b2Vec2 ip1 = in[i][(in[i].size() - 1)];
			size_t iniSize = in[i].size();
			for(size_t j = 0; j < iniSize; ++j) {
				b2Vec2 ip2 = in[i][j];
				b2Vec2 op1 = poly[0];
				for(size_t k = poly.size(); k-- > 0;) {
					b2Vec2 op2 = poly[k];
					if(SamePoint(ip1, op1) && SamePoint(ip2, op2)) {
						std::vector<b2Vec2> np = CopyRangeWrap(in[i], j + 1, in[i].size() - 2);
						//PrintPoly(poly, "Current Poly");
						//PrintPoly(np, "New Points");
						poly.insert(poly.begin() + Wrap(k + 1, 0, poly.size()), np.begin(), np.end());
						//PrintPoly(poly, "New Poly", 2);
						j = iniSize;
						in.pop_back();
						break;
					}
					op1 = op2;
				}
				ip1 = ip2;
			}
		}
		out.push_back(poly);
	}
	in = out;
}

bool GetShapeVerts(b2Body* in, std::vector<std::vector<b2Vec2> >& out) {
	b2Fixture* fix = in->GetFixtureList();
	uint32 fixCount = GetFixtureCount(fix);
	switch(fixCount) {
	case 0:
		return false;
	default:
		b2BodyUserData* bud = GetUserData(in);
		if(bud->allFixes.size() == fixCount) {
			out = bud->allFixes;
		} else {
			out = std::vector<std::vector<b2Vec2> >(fixCount);
			for(uint32 i = 0; i < fixCount && fix != NULL; ++i) {
				if(!GetShapeVerts(fix, out[i])) {
					return false;
				}
				fix = fix->GetNext();
			}
			bud->allFixes = out;
		}
		return true;
	}
}

void GetWorldPoints(b2Body* in, std::vector<b2Vec2>& out) {
	for(int32 i = 0; i < out.size(); i++) {
		out[i] = in->GetWorldPoint(out[i]);
	}
}

//http://rosettacode.org/wiki/Sutherland-Hodgman_polygon_clipping#JavaScript
//Note that this only works when fA is a convex polygon, but we know all 
//fixtures in Box2D are convex, so that will not be a problem
bool findIntersectionOfFixtures(b2Fixture* fA, b2Body* fB, std::vector<std::vector<b2Vec2> >& outputVertices) {

	std::vector<b2Vec2> clipPolygon;

	if(!GetShapeVerts(fA, clipPolygon)) {
		return false;
	}
	GetWorldPoints(fA->GetBody(), clipPolygon);

	if(!GetShapeVerts(fB, outputVertices)) {
		return false;
	}

	bool b = false;

	for(int32 i = 0; i < outputVertices.size(); i++) {
		GetWorldPoints(fB, outputVertices[i]);
		b2Vec2 cp1 = clipPolygon[clipPolygon.size() - 1];
		for(int32 j = 0; j < clipPolygon.size(); j++) {
			b2Vec2 cp2 = clipPolygon[j];
			if(outputVertices[i].empty()) {
				break;
			}
			std::vector<b2Vec2> inputList = outputVertices[i];
			outputVertices[i].clear();
			b2Vec2 s = inputList[inputList.size() - 1]; //last on the input list
			for(int32 k = 0; k < inputList.size(); k++) {
				b2Vec2 e = inputList[k];
				if(inside(cp1, cp2, e)) {
					if(!inside(cp1, cp2, s)) {
						outputVertices[i].push_back(intersection(cp1, cp2, s, e));
					}
					outputVertices[i].push_back(e);
				} else if(inside(cp1, cp2, s)) {
					outputVertices[i].push_back(intersection(cp1, cp2, s, e));
				}
				s = e;
			}
			cp1 = cp2;
		}
		if(outputVertices[i].empty()) {
			outputVertices.erase(outputVertices.begin() + i--);
			continue;
		}
		b = true;
	}

	return b;
}

//random number between 0 and 1
float32 rnd_1() {
	return rand() / (float32)RAND_MAX;
}

void UpdateBuoyancy(std::set<fixturePair>& m_fixturePairs, Settings* settings) {
	//go through all buoyancy fixture pairs and apply necessary forces
	std::set<fixturePair>::iterator it = m_fixturePairs.begin();
	std::set<fixturePair>::iterator end = m_fixturePairs.end();
	while(it != end) {

		//fixtureA is the fluid
		b2Fixture* fixtureA = it->first;
		b2Body* fixtureB = it->second;

		float32 density = fixtureA->GetDensity();

		std::vector<std::vector<b2Vec2> > intersectionPoints;
		if(findIntersectionOfFixtures(fixtureA, fixtureB, intersectionPoints)) {

			//find centroid
			float32 area = 0.0f;
			b2Vec2 centroid = ComputeCentroid(intersectionPoints, area);

			//apply buoyancy force
			float32 displacedMass = density * area;
			b2Vec2 gravity = fixtureA->GetBody()->GetWorld()->GetGravity();
			b2Vec2 buoyancyForce = displacedMass * -gravity;
#ifdef USE_LINEAR_IMPULSE
			fixtureB->ApplyLinearImpulse((1.0f / settings->hz) * buoyancyForce, centroid, true);
#else
			fixtureB->ApplyForce(buoyancyForce, centroid, true);
#endif
			//apply complex drag
			float32 dragMod = 1.0f;//adjust as desired
			float32 liftMod = 1.0f;//adjust as desired

			
			float32 maxDrag = 2000;
			float32 maxLift = 500;

			MergePolys(intersectionPoints);

			for(int i = 0; i < intersectionPoints.size(); i++) {
				
				for(int j = 0; j < intersectionPoints[i].size(); j++) {
					b2Vec2 v0 = intersectionPoints[i][j];
					b2Vec2 v1 = intersectionPoints[i][(j + 1) % intersectionPoints[i].size()];
					b2Vec2 midPoint(0.5f*(v0.x + v1.x), 0.5f*(v0.y + v1.y));

					//find relative velocity between object and fluid at edge midpoint
					b2Vec2 velDir = fixtureB->GetLinearVelocityFromWorldPoint(midPoint) -
						fixtureA->GetBody()->GetLinearVelocityFromWorldPoint(midPoint);
					float32 vel = velDir.Normalize();

					b2Vec2 edge = v1 - v0;
					float32 edgeLength = edge.Normalize();
					b2Vec2 normal = b2Cross(-1.0f, edge);
					float32 dragDot = b2Dot(normal, velDir);
					if(dragDot < 0)
						continue;//normal points backwards - this is not a leading edge

					//apply drag
					float32 dragMag = dragMod * dragDot * edgeLength * density * vel * vel;

					dragMag *= fixtureA->GetFriction();

					dragMag = b2Min(dragMag, maxDrag);

					b2Vec2 dragForce = dragMag * -velDir;
#ifdef USE_LINEAR_IMPULSE
					fixtureB->ApplyLinearImpulse(1.0f / settings->hz * dragForce, midPoint, true);
#else
					fixtureB->ApplyForce(dragForce, midPoint, true);
#endif
				
					//apply lift
					float32 liftDot = b2Dot(edge, velDir);
					float32 liftMag = liftMod * (dragDot * liftDot) * edgeLength * density * vel * vel;

					liftMag *= fixtureA->GetFriction();

					liftMag = b2Min(liftMag, maxLift);

					b2Vec2 liftDir = b2Cross(1, velDir);
					b2Vec2 liftForce = liftMag * liftDir;
#ifdef USE_LINEAR_IMPULSE
					fixtureB->ApplyLinearImpulse(1.0f / settings->hz * liftForce, midPoint, true);
#else
					fixtureB->ApplyForce(liftForce, midPoint, true);
#endif
				
				}
			}
		}

		++it;
	}
}

#endif









