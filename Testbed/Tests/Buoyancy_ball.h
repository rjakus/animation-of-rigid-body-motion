
#ifndef BUOYANCY_BALL_H
#define BUOYANCY_BALL_H

#include "iforce2d_Buoyancy_functions.h"

class Buoyancy_ball : public Test {
public:
	Buoyancy_ball() {
		//include dumped world data
		#include "buoyancy.cpp"
	}

	std::set<fixturePair> m_fixturePairs;

	void BeginContact(b2Contact* contact) {
		b2Fixture* fixtureA = contact->GetFixtureA();
		b2Fixture* fixtureB = contact->GetFixtureB();
		b2Body* bodyA = fixtureA->GetBody();
		b2Body* bodyB = fixtureB->GetBody();

		//This assumes every sensor fixture is fluid, and will interact
		//with every dynamic body.
		if(fixtureA->IsSensor() && IsFluid(fixtureA) &&
		   bodyB->GetType() == b2_dynamicBody) {
			//fixturePair pair = std::make_pair(fixtureA, bodyB);
			if(AddFluidContact(fixtureA, bodyB)) {//m_fixturePairs.find(pair) != m_fixturePairs.end() &&
				m_fixturePairs.insert(std::make_pair(fixtureA, bodyB));
			}
		} else if(fixtureB->IsSensor() && IsFluid(fixtureB) &&
				  bodyA->GetType() == b2_dynamicBody) {
			if(AddFluidContact(fixtureB, bodyA)) {
				m_fixturePairs.insert(std::make_pair(fixtureB, bodyA));
			}
		}
	}

	void EndContact(b2Contact* contact) {
		b2Fixture* fixtureA = contact->GetFixtureA();
		b2Fixture* fixtureB = contact->GetFixtureB();
		b2Body* bodyA = fixtureA->GetBody();
		b2Body* bodyB = fixtureB->GetBody();

		//This check should be the same as for BeginContact, but here
		//we remove the fixture pair
		if(fixtureA->IsSensor() && IsFluid(fixtureA) &&
		   bodyB->GetType() == b2_dynamicBody) {
			if(RemoveFluidContact(fixtureA, bodyB)) {
				m_fixturePairs.erase(std::make_pair(fixtureA, bodyB));
			}
		} else if(fixtureB->IsSensor() && IsFluid(fixtureB) &&
				  bodyA->GetType() == b2_dynamicBody) {
			if(RemoveFluidContact(fixtureB, bodyA)) {
				m_fixturePairs.erase(std::make_pair(fixtureB, bodyA));
			}
		}
	}



	void Step(Settings* settings) {
		Test::Step(settings);
		UpdateBuoyancy(m_fixturePairs, settings);


	}

	static Test* Create() {
		return new Buoyancy_ball;
	}
};

#endif
