
b2Vec2 g(0.000000000000000e+00f, -1.000000000000000e+01f);
m_world->SetGravity(g);
b2Body** bodies = (b2Body**)b2Alloc(18 * sizeof(b2Body*));
b2Joint** joints = (b2Joint**)b2Alloc(0 * sizeof(b2Joint*));

//donja strana kutije
{
  b2BodyDef bd;
  bd.type = b2BodyType(0);
  bd.position.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angle = 0.000000000000000e+00f;
  bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angularVelocity = 0.000000000000000e+00f;
  bd.linearDamping = 0.000000000000000e+00f;
  bd.angularDamping = 0.000000000000000e+00f;
  bd.allowSleep = bool(4);
  bd.awake = bool(2);
  bd.fixedRotation = bool(0);
  bd.bullet = bool(0);
  bd.active = bool(32);
  bd.gravityScale = 1.000000000000000e+00f;
  bodies[1] = m_world->CreateBody(&bd);

  {
    b2FixtureDef fd;
    fd.friction = 2.000000029802322e-01f;
    fd.restitution = 0.000000000000000e+00f;
    fd.density = 1.000000000000000e+00f;
    fd.isSensor = bool(0);
    fd.filter.categoryBits = uint16(1);
    fd.filter.maskBits = uint16(65535);
    fd.filter.groupIndex = int16(0);
    b2PolygonShape shape;
    b2Vec2 vs[8];
    vs[0].Set(2.000000000000000e+01f, -5.000000000000000e-01f);
    vs[1].Set(2.000000000000000e+01f, 5.000000000000000e-01f);
    vs[2].Set(-5.485061645507812e+01f, 5.000000000000000e-01f);
    vs[3].Set(-5.485061645507812e+01f, -5.000000000000000e-01f);
    shape.Set(vs, 4);

    fd.shape = &shape;

    bodies[1]->CreateFixture(&fd);
  }
}


//desna strana kutije
{
  b2BodyDef bd;
  bd.type = b2BodyType(0);
  bd.position.Set(2.000000000000000e+01f, 1.999999809265137e+01f);
  bd.angle = -1.570796370506287e+00f;
  bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angularVelocity = 0.000000000000000e+00f;
  bd.linearDamping = 0.000000000000000e+00f;
  bd.angularDamping = 0.000000000000000e+00f;
  bd.allowSleep = bool(4);
  bd.awake = bool(2);
  bd.fixedRotation = bool(0);
  bd.bullet = bool(0);
  bd.active = bool(32);
  bd.gravityScale = 1.000000000000000e+00f;
  bodies[2] = m_world->CreateBody(&bd);

  {
    b2FixtureDef fd;
    fd.friction = 2.000000029802322e-01f;
    fd.restitution = 0.000000000000000e+00f;
    fd.density = 1.000000000000000e+00f;
    fd.isSensor = bool(0);
    fd.filter.categoryBits = uint16(1);
    fd.filter.maskBits = uint16(65535);
    fd.filter.groupIndex = int16(0);
    b2PolygonShape shape;
    b2Vec2 vs[8];
    vs[0].Set(2.000000000000000e+01f, -5.000000000000000e-01f);
    vs[1].Set(2.000000000000000e+01f, 5.000000000000000e-01f);
    vs[2].Set(-2.000000000000000e+01f, 5.000000000000000e-01f);
    vs[3].Set(-2.000000000000000e+01f, -5.000000000000000e-01f);
    shape.Set(vs, 4);

    fd.shape = &shape;

    bodies[2]->CreateFixture(&fd);
  }
}
//lijeva strana kutije
{
  b2BodyDef bd;
  bd.type = b2BodyType(0);
  bd.position.Set(-2.000000000000000e+01f, 2.000000000000000e+01f);
  bd.angle = -1.570796370506287e+00f;
  bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angularVelocity = 0.000000000000000e+00f;
  bd.linearDamping = 0.000000000000000e+00f;
  bd.angularDamping = 0.000000000000000e+00f;
  bd.allowSleep = bool(4);
  bd.awake = bool(2);
  bd.fixedRotation = bool(0);
  bd.bullet = bool(0);
  bd.active = bool(32);
  bd.gravityScale = 1.000000000000000e+00f;
  bodies[3] = m_world->CreateBody(&bd);

  {
    b2FixtureDef fd;
    fd.friction = 2.000000029802322e-01f;
    fd.restitution = 0.000000000000000e+00f;
    fd.density = 1.000000000000000e+00f;
    fd.isSensor = bool(0);
    fd.filter.categoryBits = uint16(1);
    fd.filter.maskBits = uint16(65535);
    fd.filter.groupIndex = int16(0);
    b2PolygonShape shape;
    b2Vec2 vs[8];
    vs[0].Set(2.000000190734863e+01f, -3.535061645507812e+01f);
    vs[1].Set(2.000000190734863e+01f, -3.435061645507812e+01f);
    vs[2].Set(-1.999999809265137e+01f, -3.435061645507812e+01f);
    vs[3].Set(-1.999999809265137e+01f, -3.535061645507812e+01f);
    shape.Set(vs, 4);

    fd.shape = &shape;

    bodies[3]->CreateFixture(&fd);
  }
}

//gornji dio kutije
{
  b2BodyDef bd;
  bd.type = b2BodyType(0);
  bd.position.Set(0.000000000000000e+00f, 4.000000000000000e+01f);
  bd.angle = 0.000000000000000e+00f;
  bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angularVelocity = 0.000000000000000e+00f;
  bd.linearDamping = 0.000000000000000e+00f;
  bd.angularDamping = 0.000000000000000e+00f;
  bd.allowSleep = bool(4);
  bd.awake = bool(2);
  bd.fixedRotation = bool(0);
  bd.bullet = bool(0);
  bd.active = bool(32);
  bd.gravityScale = 1.000000000000000e+00f;
  bodies[4] = m_world->CreateBody(&bd);

  {
    b2FixtureDef fd;
    fd.friction = 2.000000029802322e-01f;
    fd.restitution = 0.000000000000000e+00f;
    fd.density = 1.000000000000000e+00f;
    fd.isSensor = bool(0);
    fd.filter.categoryBits = uint16(1);
    fd.filter.maskBits = uint16(65535);
    fd.filter.groupIndex = int16(0);
    b2PolygonShape shape;
    b2Vec2 vs[8];
    vs[0].Set(2.000000000000000e+01f, -5.000000000000000e-01f);
    vs[1].Set(2.000000000000000e+01f, 5.000000000000000e-01f);
    vs[2].Set(-5.485061645507812e+01f, 5.000000000000000e-01f);
    vs[3].Set(-5.485061645507812e+01f, -5.000000000000000e-01f);
    shape.Set(vs, 4);

    fd.shape = &shape;

    bodies[4]->CreateFixture(&fd);
  }
}
//bazen
{
  b2BodyDef bd;
  bd.type = b2BodyType(0);
  bd.position.Set(2.112629318237305e+01f, 1.750797271728516e+00f);
  bd.angle = 0.000000000000000e+00f;
  bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angularVelocity = 0.000000000000000e+00f;
  bd.linearDamping = 0.000000000000000e+00f;
  bd.angularDamping = 0.000000000000000e+00f;
  bd.allowSleep = bool(4);
  bd.awake = bool(2);
  bd.fixedRotation = bool(0);
  bd.bullet = bool(0);
  bd.active = bool(32);
  bd.gravityScale = 1.000000000000000e+00f;
  bodies[5] = m_world->CreateBody(&bd);

  {
    b2FixtureDef fd;
    fd.friction = 2.000000029802322e-01f;
    fd.restitution = 0.000000000000000e+00f;
    fd.density = 2.000000000000000e+00f;
    fd.isSensor = bool(1);
    fd.filter.categoryBits = uint16(1);
    fd.filter.maskBits = uint16(65535);
    fd.filter.groupIndex = int16(0);
    b2PolygonShape shape;
    b2Vec2 vs[8];
    vs[0].Set(-5.544052124023438e-01f, -1.515119433403015e+00f);
    vs[1].Set(-5.544052124023438e-01f, 7.516476631164551e+00f);
    vs[2].Set(-4.341683959960938e+01f, 7.516476631164551e+00f);
    vs[3].Set(-4.341683959960938e+01f, -1.515119433403015e+00f);
    shape.Set(vs, 4);

    fd.shape = &shape;

    b2FixtureUserData* fud = (b2FixtureUserData*)malloc(sizeof(b2FixtureUserData));
    b2Assert(fud != NULL);
    fud->isFluid = true;

    fd.userData = (void*)fud;

    bodies[5]->CreateFixture(&fd);
  }
}

//rampa
{
  b2BodyDef bd;
            bd.position.Set(-1.112629318237305e+01f, 1.750797271728516e+00f);
            b2Body* body = m_world->CreateBody(&bd);

            b2EdgeShape edge[4];

            edge[0].Set(b2Vec2(-43.0f, 12.516476631164551e+00f), b2Vec2(-11.0f, 12.516476631164551e+00f));
            body->CreateFixture(&edge[0], 0.0f);
}





//ball
{
	
            b2BodyDef myBodyDef1;
            myBodyDef1.type = b2_dynamicBody; //this will be a dynamic body
            myBodyDef1.position.Set(-50, 17); //a little to the left
            myBodyDef1.angle = 0; //set the starting angle

            bodies[6] = m_world->CreateBody(&myBodyDef1);

            b2CircleShape circleShape;
            //circleShape.m_p.Set(0, 0); //position, relative to body position
            circleShape.m_radius = 1; //radius

            b2FixtureDef myFixtureDef;
            myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
            myFixtureDef.density = 1.0f;
            myFixtureDef.friction = 0.7f; //new cod koliko ce se okretati
            myFixtureDef.restitution = 0.8f; //za oskakivanje

            bodies[6]->CreateFixture(&myFixtureDef); //add a fixture to the body
            
            
            int degrees=70, power=50;
            bodies[6]->ApplyLinearImpulse( b2Vec2(cos(degrees * (PI / 180)) * power,sin(degrees * (PI / 180)) * power),bodies[6]->GetWorldCenter(), false);
 

}
{
	
            b2BodyDef myBodyDef1;
            myBodyDef1.type = b2_dynamicBody; //this will be a dynamic body
            myBodyDef1.position.Set(15, 22); //a little to the left
            myBodyDef1.angle = 0; //set the starting angle

            bodies[7] = m_world->CreateBody(&myBodyDef1);

            b2CircleShape circleShape;
            //circleShape.m_p.Set(0, 0); //position, relative to body position
            circleShape.m_radius = 2; //radius

            b2FixtureDef myFixtureDef;
            myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
            myFixtureDef.density = 1.0f;
            myFixtureDef.friction = 0.7f; //new cod koliko ce se okretati
            myFixtureDef.restitution = 0.5f; //za oskakivanje

            bodies[7]->CreateFixture(&myFixtureDef); //add a fixture to the body
            

}
//kockica
{ 
  b2BodyDef bd;
  bd.type = b2BodyType(2);
  bd.position.Set(11, 3);
  bd.angle = 0.000000000000000e+00f;
  bd.linearVelocity.Set(0.000000000000000e+00f, 0.000000000000000e+00f);
  bd.angularVelocity = 0.000000000000000e+00f;
  bd.linearDamping = 0.000000000000000e+00f;
  bd.angularDamping = 0.000000000000000e+00f;
  bd.allowSleep = bool(4);
  bd.awake = bool(2);
  bd.fixedRotation = bool(0);
  bd.bullet = bool(0);
  bd.active = bool(32);
  bodies[8] = m_world->CreateBody(&bd);

  {
    b2FixtureDef fd;
    fd.friction = 2.000000029802322e-01f;
    fd.restitution = 0.000000000000000e+00f;
    fd.density = 1.000000000000000e+00f;
    fd.isSensor = bool(0);
    fd.filter.categoryBits = uint16(1);
    fd.filter.maskBits = uint16(65535);
    fd.filter.groupIndex = int16(0);
    b2PolygonShape shape;
    b2Vec2 vs[8];
    vs[0].Set(1.000000000000000e+00f, -1.000000000000000e+00f);
    vs[1].Set(1.000000000000000e+00f, 1.000000000000000e+00f);
    vs[2].Set(-1.000000000000000e+00f, 1.000000000000000e+00f);
    vs[3].Set(-1.000000000000000e+00f, -1.000000000000000e+00f);
    shape.Set(vs, 4);

    fd.shape = &shape;

    bodies[8]->CreateFixture(&fd);
  }
}

b2Free(joints);
b2Free(bodies);
joints = NULL;
bodies = NULL;

