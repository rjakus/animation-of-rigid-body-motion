
#include "../Framework/Test.h"
#include "../Framework/Render.h"

#ifdef __APPLE__
	#include <GLUT/glut.h>
#else
	#include "freeglut/freeglut.h"
#endif

#include "ball_wind.h"
#include "Buoyancy_ball.h"

#

TestEntry g_testEntries[] =
{   
	{"ball_wind", ball_wind::Create},
	{"Buoyancy_ball", Buoyancy_ball::Create},
	
	{NULL, NULL}
};
